import 'dart:async';

import 'package:baseplateflutter/blocs/authentication/authentication_bloc.dart';
import 'package:baseplateflutter/repositories/repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final Repository repository;
  final AuthenticationBloc authenticationBloc;

  RegisterBloc({
    @required this.repository,
    @required this.authenticationBloc,
  });

  RegisterState get initialState => RegisterInitial();

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterButtonPressed) {
      print('hello');
      yield RegisterLoading();

      try {
        final token = await repository.register(
          email: event.email,
          password: event.password,
          firstName: event.firstName,
          lastName: event.lastName,
        );

        authenticationBloc.add(LoggedIn(token: token));
        yield RegisterInitial();
      } catch (err) {
        final String message = err['message'];
        yield RegisterFailure(error: message.toString());
      }
    }
  }
}
