import 'package:baseplateflutter/blocs/authentication/authentication_bloc.dart';
import 'package:baseplateflutter/blocs/register/register_bloc.dart';
import 'package:baseplateflutter/components/register_form.dart';
import 'package:baseplateflutter/pages/login_page.dart';
import 'package:baseplateflutter/repositories/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegisterPage extends StatelessWidget {
  static const String id = 'register_page';

  final Repository repository;
  RegisterPage({Key key, @required this.repository});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
      ),
      body: BlocProvider(
        create: (context) {
          return RegisterBloc(
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
            repository: repository,
          );
        },
        child: RegisterForm(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Text('L'),
        onPressed: () {
          Navigator.pushReplacementNamed(context, LoginPage.id);
        },
      ),
    );
  }
}
